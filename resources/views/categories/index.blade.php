@extends('layouts.app')
@section('content')
    <ul class="list-group align-items-center ">
        @foreach ($categories as $category)
            <li class="list-group-item">
                {{$category->name}}
                <a href="categories/{{$category->id}}/edit">edit</a>
                <a href="categories/{{$category->id}}/delete">delete</a>
            </li> 
        @endforeach
    </ul>
@endsection

