@extends('layouts.app')
@section('content')
<form action="/categories/store-categories"  method="POST">
  @csrf
    <div class="form-group">
      <label for="name">Name for category</label>
      <input type="input" name='name' class="form-control" id="name">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection

