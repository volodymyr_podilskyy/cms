<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Category;
 
class CategoriesController extends Controller
{
    public function index() 
    {
        $categories = Category::all();
        return view('categories.index', ['categories' => $categories]);
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store()
    {
        request()->validate([
            'name' => 'required|min:4|max:20'
        ]);
        $data = request()->all();
        $category = new Category();
        $category->name = $data['name'];
        $category->save();
        return redirect('categories');
    }

    public function delete(Category $category)
    {
        $category->delete();
        return redirect('categories');
    }

    public function edit(Category $category)
    {
        return view('categories.edit', ['category' => $category]);
    }

    public function update(Category $category)
    {
        request()->validate([
            'name' => 'required|min:4|max:20'
        ]);
        $data = request()->all();
        $category->name = $data['name'];
        $category->save();
        return redirect('categories');
    }
}
